import {Component} from '@angular/core';
import {ErrorHandler} from "@angular/core";
import axios from "axios";
import {AxiosInstance} from "axios";
import {Item} from "./item";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  axiosClient: AxiosInstance;
  errorHandler: ErrorHandler;

  constructor(errorHandler: ErrorHandler) {
    this.errorHandler = errorHandler;
    this.axiosClient = axios.create({
      timeout: 3000,
      headers: {
        "X-Initialized-At": Date.now().toString()
      }
    });
  }

  public testfunc() {
    this.axiosClient.get('http://10.1.1.147:9606/api/bso/test').then(function (result) {
      console.log(result.data);
    });
  }

  allItems = [] as Array<Item>;
  get items() {
    return this.allItems;
  }
  addItem(description: any) {
    // 新增物件
    this.allItems.unshift({
      description,
      done: false
    });
  }
  remove(item: { description: string; done: boolean; }) {
    this.allItems.splice(this.allItems.indexOf(item), 1);
  }

}
